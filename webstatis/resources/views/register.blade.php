<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="post">
        @csrf
      <label>First name:</label>
      <br /><br />
      <input type="text" name="namdep" />
      <br /><br />
      <label>Last name:</label>
      <br /><br />
      <input type="text" name="nambel" />
      <br /><br />
      <label>Gender:</label>
      <br /><br />
      <input type="radio" name="gender" />Male
      <br />
      <input type="radio" name="gender" />Female
      <br />
      <input type="radio" name="gender" />Other <br /><br />
      <label>Nationality:</label>
      <br /><br />
      <select name="nationality">
        <option value="indonesian">Indonesian</option>
        <br />
        <option value="australian">Australian</option>
        <br />
        <option value="american">American</option>
        <br /><br />
      </select>
      <br /><br />
      <label>Language Spoken:</label>
      <br /><br />
      <input type="checkbox" name="language" />Bahasa Indonesia
      <br />
      <input type="checkbox" name="language" />Bahasa English
      <br />
      <input type="checkbox" name="language" />Other <br /><br />
      <label>Bio:</label>
      <br /><br />
      <textarea name="bio" cols="30" rows="10"></textarea>
      <br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
